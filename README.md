compress_mov -- batch convert Canon MOV videos to 720p x264 MP4

compress_mov is a thin wrapper around HandBrakeCLI to batch convert Canon MOV videos to a more compressed format.
The current settings convert the video to 720p (1280 x 720 pixels). It compresses the video stream with h264 codec high profile 4.1
(Handbrake 'quality' parameter set to 19) and compresses the audio stream with ac3 codec at 128 kbit/s.
With these settings, file size is roughly divided by 10. 

HandBrakeCLI must be installed for this script to work. Download it from http://handbrake.fr/

