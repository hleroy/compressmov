#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
compress_mov -- batch convert Canon MOV videos to 720p x264 MP4

compress_mov is a thin wrapper around HandBrakeCLI to batch convert Canon MOV videos to a more
compressed format. The current settings convert the video to 720p (1280 x 720 pixels).
It compresses the video stream with h264 codec high profile 4.1 (Handbrake 'quality' parameter
set to 19) and compresses the audio stream with ac3 codec at 128 kbit/s.
With these settings, file size is roughly divided by 10.

HandBrakeCLI must be installed for this script to work. Download it from http://handbrake.fr/

@author:     Hervé Le Roy
@contact:    hleroy@hleroy.com
'''

import os
import sys
import subprocess

from signal import *  # @UnusedWildImport
from argparse import ArgumentParser

HANDBRAKE_CLI_EXECUTABLE = "HandBrakeCLI"
HANDBRAKE_ARGS = ['-t', '1', '--angle', '1', '-c', '1', '-f', 'm4v', '-4', '-O', '--decomb',
                  '-w', '1280', '--loose-anamorphic', '--modulus', '2', '-e', 'x264', '-q', '19',
                  '--vfr', '-a', '1', '-E', 'faac', '-6', 'dpl2', '-R', '48', '-B', '128', '-D',
                  '0', '--gain', '0', '--audio-fallback', 'ffac3', '--x264-preset=placebo',
                  '--x264-profile=high', '--x264-tune=film', '--h264-level=4.1']


# Credit: http://stackoverflow.com/questions/11210104/check-if-a-program-exists-from-a-python-script
def is_tool(name):
    """Check if a program exists."""
    try:
        devnull = open(os.devnull)
        subprocess.Popen([name], stdout=devnull, stderr=devnull).communicate()
    except OSError as e:
        if e.errno == os.errno.ENOENT:
            return False
    return True


# Credit: http://stackoverflow.com/questions/3041986/python-command-line-yes-no-input
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    valid = {"yes": "yes", "y": "yes", "ye": "yes", "no": "no", "n": "no"}
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "\
                              "(or 'y' or 'n').\n")


# Credit: http://code.activestate.com/recipes/577081-humanized-representation-of-a-number-of-bytes
def humanize_bytes(nb_bytes, precision=1):
    """Return a humanized string representation of a number of bytes."""
    abbrevs = (
        (1 << 50, 'PB'),
        (1 << 40, 'TB'),
        (1 << 30, 'GB'),
        (1 << 20, 'MB'),
        (1 << 10, 'kB'),
        (1, 'bytes')
    )
    if nb_bytes == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if nb_bytes >= factor:
            break
    return '%.*f %s' % (precision, nb_bytes / factor, suffix)


def cleanup(*args):
    print('\nYou pressed Ctrl+C!')
    sys.exit(0)


def main():

    # Handle Ctrl-C interrupt or any other OS interrupt
    for sig in (SIGABRT, SIGILL, SIGINT, SIGSEGV, SIGTERM):
        signal(sig, cleanup)

    # Check if HandBrakeCLI is installed
    if not is_tool(HANDBRAKE_CLI_EXECUTABLE):
        sys.exit("ERROR: %s is not installed. Download it from http://handbrake.fr/"
                 % HANDBRAKE_CLI_EXECUTABLE)

    # Setup argument parser
    parser = ArgumentParser(description="batch convert Canon MOV videos to 720p x264 MP4")
    parser.add_argument("directory", nargs="?", default=os.getcwd(),
                        help="directory where to find MOV files. "
                             "if omitted, starts within the current directory")
    parser.add_argument("-n", "--dry-run", action="store_true",
                        help="search for MOV files but do not convert them to MP4")
    args = parser.parse_args()

    # Check if the provided argument is a directory
    start = args.directory
    if not os.path.isdir(start):
        sys.exit("ERROR: %s is not a directory!\nType \u00AB %s --help \u00BB for instructions"
                 % (start, sys.argv[0]))

    print("Searching MOV files in directory " + start)

    # Recursively walk through all files within start directory, including sub-directories,
    # counting all MOV files and calculating the total filesize to be processed
    count = size = 0
    for root, dirs, files in os.walk(start):  # @UnusedVariable
        for name in files:

        # Get the extension
            (base, ext) = os.path.splitext(name)

            # If it is a .mov file
            if ext.lower() == ".mov":
                infile = os.path.join(root, name)
                outfile = os.path.join(root, base + ".MP4")

                # If outfile doesn't exists, increase file count and size
                if not os.path.exists(outfile):
                    count += 1
                    size += os.path.getsize(infile)

    # If there are some MOV files to process, print file count and total file size. Otherwise exit.
    if count != 0:
        print("%d MOV file%s to process (%s)" % (count, "s"[count == 1:], humanize_bytes(size)))
    else:
        print("No MOV files to process, exiting")
        sys.exit()

    # Prompt user if he is ready to start encoding
    if query_yes_no("Start encoding ?") == "yes":

        # Recursively walk through all files within start directory, including sub-directories,
        # launching Handbrake for every MOV file that hasn't yet been processed
        for root, dirs, files in os.walk(start):  # @UnusedVariable
            for name in files:

                # Get the extension
                (base, ext) = os.path.splitext(name)

                # If it is a .mov file
                if ext.lower() == ".mov":
                    infile = os.path.join(root, name)
                    outfile = os.path.join(root, base + ".MP4")
                    tmpfile = outfile + ".part"

                    # Test if outfile exists
                    if os.path.exists(outfile):

                        # Skip if outfile already exists
                        print("Skipping " + infile + " (MP4 file already exists)")

                    else:

                        # Print file name and size
                        print("Processing " + infile + " ("
                              + humanize_bytes(os.path.getsize(infile)) + ") ")

                        # If this is not a dry run, launch the encoding !
                        if not args.dry_run:

                            try:

                                # Call HandBrakeCLI
                                proc = subprocess.Popen(["HandBrakeCLI", "-i", infile, "-o",
                                                         tmpfile] + HANDBRAKE_ARGS,
                                                        bufsize=1,
                                                        stdout=subprocess.PIPE,
                                                        stderr=subprocess.STDOUT,
                                                        universal_newlines=True)

                                # Monitor STDOUT and print progress
                                while proc.poll() is None:
                                    line = proc.stdout.readline()
                                    # Handbrake prints progress like this:
                                    # Encoding: task 1 of 1, 0.15 %
                                    # If the line contains "Encoding:", we capture the progress
                                    # percentage and print it in place using a carriage return (\r)
                                    # at the beginning of the line
                                    if 'Encoding:' in line:
                                        progress = line[23:30]
                                        sys.stdout.write("\r%s" % (progress))
                                        sys.stdout.flush()
                                # Print another carriage return (\r) so that the next print
                                # statement overwrites the progress percentage
                                sys.stdout.write("\r")
                                sys.stdout.flush()

                                # Rename temp file (.MP4.part) to .MP4
                                os.rename(tmpfile, outfile)

                            finally:
                                # Cleanup temp file in case of interrupt
                                if os.path.exists(tmpfile):
                                    print("Cleaning up...")
                                    os.remove(tmpfile)
                                    print("Done")

        print("Finished !")

    else:
        print("Aborting...")


if __name__ == "__main__":
    main()
